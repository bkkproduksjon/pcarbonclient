import os
import time # for time.sleep()
from selenium import webdriver
import timefuncs as t
from timefuncs import timenow
#from pyvirtualdisplay import Display
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException

def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

user = os.environ.get('API_USER')
passw = os.environ.get('API_PASS')

options = Options()
#options.gpu = False
#options.headless = True
#prefs = {"download.default_directory": "", "download.prompt_for_download": False, "download.directory_upgrade": True, "safebrowsing.enabled": True}
#options.add_experimental_option("prefs", prefs)
#desired = options.to_capabilities()

# Open chromdriver headlessly...
#display = Display(visible=0, size(800,600))
#display.start()
#driver = webdriver.Chrome(desired_capabilities = desired)
driver = webdriver.Chrome()

# Navigate to Thomson Reuters and sign in
driver.get('https://emea1.apps.cp.thomsonreuters.com/web/cms/?navid=37873879')

username_box = driver.find_element_by_xpath("//input[@name='IDToken1']").send_keys(user)
pass_box = driver.find_element_by_xpath("//input[@name='IDToken2']").send_keys(passw)
signin_btn = driver.find_element_by_xpath("//div[@class='buttonarea']/div[1]").click()

# Test if Thomson Reuters tells you that you're already signed in elsewhere and prompts to sign in again
reSignInBtn = "//form[@name='frmSignIn']/div[2]"
if check_exists_by_xpath(reSignInBtn):
    signin_btn = driver.find_element_by_xpath(reSignInBtn)
signin_btn.click()

# excel files download URIs (need an up to date UNIX time stamp)
consChart = '100081643'
windChart = '100081645'
dato = t.dateStringMidnight()
chartServerUri = 'https://emea1.apps.cp.thomsonreuters.com/web/pointcarbon/ChartServer/DownloadChartCached?'
fileType = 'csv' # also try json or csv

nrd_cons_da_uri = chartServerUri + 'id=' + consChart + '&today=' + dato + '&type=' + fileType + '&_=' + str(timenow())
nrd_cons_da_uri
driver.get(nrd_cons_da_uri)
time.sleep(5)
nrd_wnd_da_uri = chartServerUri + 'id=' + windChart + '&today=' + dato + '&type=' + fileType + '&_=' + str(timenow())
driver.get(nrd_wnd_da_uri)

#display.stop()
driver.quit()
