import datetime
import time
import os  # needed for saving file
import errno # needed for exception handling in save_ps
import json # read json data
import sys # stderr and exit

# loads config file for wapi client
configfile = 'PointCfetcher_config.txt'
try:
    configs = json.load(open(configfile, 'r'))
except IOError as e:
    sys.stderr.write("File " + configfile + " can't be opened")
    sys.exit(1)

# returns a value from config dict.
# INPUT: string key - a dict key
def get_config(key):
    return configs[key]

# reads input file and returns an array of dicts, each one describing a curve
def read_inputf():
    inputfile = get_config("curves_input_file")
    try:
        curves = json.load(open(inputfile))
        writeToLog("Curve input file opened and parsed.")
        return curves
    except IOError:
        writeToLog("Cannot open input file " + inputfile)
        sys.exit(1)

# returns current time as a Unix time stamp (integer)
def timenow():
    dt = datetime.datetime.now()
    return int(time.mktime(dt.timetuple()))

# returns current date as string with time set to midnight
def dateStringMidnight():
    return datetime.datetime.today().strftime('%Y-%m-%dT00:00:00')

# Writes a message string to a log file, location of which is specified in config file
# INPUT: string message
def writeToLog(message):
    filename = configs["log_file_location"]
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    with open(filename, 'a') as log:
        log.write(dateStringTime() + ": " + message + "\n")
        log.close()

def save_as():
    sv = configs["save_as"]
